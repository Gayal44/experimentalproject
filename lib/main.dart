import 'package:flutter/material.dart';
import 'package:flutter_app/card/MyCard.dart';
import 'package:flutter_app/SecondScreen.dart';


void main() => runApp(MaterialApp(home: MyApp(), debugShowCheckedModeBanner: false,));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Standings after round 3"),
        ),
        body: Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                MyCard(
                  title: Text("Mercedes",),
                  icon: Icon(
                    Icons.favorite,
                    size: 40.0,
                    color: Colors.red,
                  ),
                ),
                MyCard(
                    title: Text("Ferrari"),
                    icon: Icon(
                      Icons.warning,
                      size: 40.0,
                      color: Colors.yellowAccent,
                    )),
                MyCard(
                    title: Text("RedBull"),
                    icon: Icon(
                      Icons.all_inclusive,
                      size: 40.0,
                      color: Colors.lightBlue,
                    )),
                StatelessBtn(),
                StatefulBtn(),
              ],
            )),
      ),
      routes: <String, WidgetBuilder>{'/SecondScreen': (BuildContext context) => SecondScreen()},
    );
  }
}

//how stateless widgets are made.
class StatelessBtn extends StatelessWidget{

  void printToConsole(){
    print('button pressed!');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: IconButton(
          icon: Icon(Icons.ac_unit),
          onPressed: printToConsole,
          iconSize: 30.0,
        ) 
    );
  }

}

//How stateful components/widgets are made.

class StatefulBtn extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return StatefulBtnState();
  }

}

class StatefulBtnState extends State<StatefulBtn>{

  var text = "";
  var count = 0;

  void changeText(){
    setState(() {
      text = "Rerendered on button press..." + count.toString();
      count++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(text.toString()),
          MaterialButton(onPressed: (){Navigator.of(context).pushNamed("/SecondScreen");}, child: Text("PRESS ME!!"), color: Colors.red,)
        ],
      ),
    );
  }

}

