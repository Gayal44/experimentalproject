import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  MyCard({this.title, this.icon}); //constructor

  final Widget title;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 20.0),
        child: Card(
          color: Colors.white30,
          child: Container(
              padding: EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  this.icon,
                  this.title,
                ],
              )),
        ));
  }
}