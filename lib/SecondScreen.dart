import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Text("Second Screen"),
              IconButton(icon: Icon(Icons.headset, color: Colors.red,size: 70.0,), onPressed: (){Navigator.pop(context);}),
            ],),
        ),
      ),
      appBar: AppBar(
        title: Text("Second Screen"),
        backgroundColor: Colors.red,
        ),
    );
  }
  
}